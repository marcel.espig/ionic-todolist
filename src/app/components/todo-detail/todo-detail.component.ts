import { Component, OnInit, Input } from '@angular/core';
import { Todo } from '../../Todo';
import { Location } from '@angular/common';


@Component({
  selector: 'app-todo-detail',
  templateUrl: './todo-detail.component.html',
  styleUrls: ['./todo-detail.component.scss'],
})
export class TodoDetailComponent implements OnInit {


  @Input() todo?: Todo;
  constructor(private location: Location) { }

  ngOnInit() {}


  goBack(): void {
    this.location.back();
  }
}
