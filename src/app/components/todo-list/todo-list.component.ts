import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { TodoService } from '../../services/todo.service';
import { Todo } from '../../Todo';


@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss'],
})
export class TodolistComponent implements OnInit {

  public todos!: Todo[];
  isDone: boolean;
  outTodo: Todo;
  date: string;

  doneTodos: Todo[];
  undoneTodos: Todo[];

  @Input()
  title: string;

  constructor(private todoService: TodoService) { }

  ngOnInit() {
    this.getTodos();
  }


  public deleteTodo(id: number) {
    this.todoService.deleteTodo(id).subscribe(
      (response: void) => {
        console.log(response);
        this.getTodos()
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
      );
  }
  
  public getTodos(): void {

    this.todoService.getTodos().subscribe(
      (response: Todo[]) => {
        this.todos = response;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public onCheckboxChange(outTodo: Todo) {
    outTodo.isDone = !outTodo.isDone;
    if (outTodo.isDone) {
      this.todoService.setStatusTrue(outTodo.id).subscribe(
        (response: Todo) => {
        },
        (error: HttpErrorResponse) => {
          alert(error.message);
        }
      );
    }
    else {
      this.todoService.setStatusFalse(outTodo.id).subscribe(
        (response: Todo) => {
        },
        (error: HttpErrorResponse) => {
          alert(error.message);
        }
      );
    }
  }

  public dateChanged(todo) {
    console.log(todo.date);
  }



  
}
