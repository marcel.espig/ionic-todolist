import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Todo } from '../../Todo';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss'],
})
export class TodoComponent implements OnInit {

  @Input()
  inTodo: Todo;
  @Output()
  outTodo: EventEmitter<Todo> = new EventEmitter<Todo>();
  @Output()
  outIsDone: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output()
  outId: EventEmitter<number> = new EventEmitter<number>();
  constructor() { }

  ngOnInit() {}

  emitTodo(todo) {
    this.outTodo.emit(todo);
    console.log(todo.isDone);
  }

  emitTodoId(id) {
    this.outId.emit(id);
    
  }

  emitTodoIsDone(isDone) {
    this.outIsDone.emit(isDone);
  }
  getColor(): string {
    if (this.inTodo.isDone) return "done";
    else return "undone";
  }
}
