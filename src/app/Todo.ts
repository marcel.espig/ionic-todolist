export interface Todo {
  task: string;
  isDone: boolean;
  id: number;
  date: string;
}

export class Todo implements Todo {
  task: string;
  isDone: boolean;
  constructor(task: string, isDone: boolean) {
    this.task = task;
    this.isDone = isDone;

  }
}
