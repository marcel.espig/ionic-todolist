import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { TodolistComponent } from '../components/todo-list/todo-list.component';
import { TodoComponent } from '../components/todo/todo.component';
import { TodoService } from '../services/todo.service';
import { Todo } from '../Todo';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  @ViewChild(TodolistComponent) todoListChild;
  @ViewChild(TodoComponent) todoChild;

  public todos!: Todo[];

  doneTodos: Todo[];
  undoneTodos: Todo[];

  title1: string = "Noch nicht erledigt:";
  title2: string = "Bereits erledigt:";


  //public testTodo: Todo = { task: "aufgaben", isDone: false, id:2, date:"test" };

  constructor(private todoService: TodoService, private todoListComponent: TodolistComponent) {
 
  }

  ngOnInit() {
    
  }


  addTodo(text: any) {
    if (text != null) {
      let newTodo: Todo = new Todo(text.value, false);
      this.todoService.addTodo(newTodo).subscribe(
        (response: Todo) => {
          console.log(response);
          this.getTodos();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
      );
      text.value = '';
    } 
  }


  public getTodos(): void {

    this.todoService.getTodos().subscribe(
      (response: Todo[]) => {
        this.todoListChild.todos = response;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public deleteAllTodos(): void {
    
    for (let todo of this.todoListChild.todos) {
      this.todoService.deleteTodo(todo.id).subscribe(
        (response: void) => {
          console.log(response);
          this.getTodos();
        },
        (error: HttpErrorResponse) => {
          alert(error.message);
        }
      );
    }
  }
}
