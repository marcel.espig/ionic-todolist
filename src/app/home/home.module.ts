import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { HomePage } from './home.page';

import { HomePageRoutingModule } from './home-routing.module';
import { TodolistComponent } from '../components/todo-list/todo-list.component';
import { HttpClientModule } from '@angular/common/http';
import { TodoService } from '../services/todo.service';
import { TodoDetailComponent } from '../components/todo-detail/todo-detail.component';
import { TodoComponent } from '../components/todo/todo.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomePageRoutingModule,
    HttpClientModule
  ],
  declarations: [HomePage, TodolistComponent, TodoDetailComponent, TodoComponent],
  providers: [TodoService, TodolistComponent]
})
export class HomePageModule {}
