import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { Todo } from '../Todo';

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  private apiServerUrl = environment.apiBaseUrl;
  constructor(private http: HttpClient) { }

  public getTodos(): Observable<Todo[]> {
    return this.http.get<Todo[]>(`${this.apiServerUrl}/todolist/all`)
  }

  public addTodo(todo: Todo): Observable<Todo> {
    return this.http.post<Todo>(`${this.apiServerUrl}/todolist/add`, todo)
  }

  public deleteTodo(id: number): Observable<void> {
    return this.http.delete<void>(`${this.apiServerUrl}/todolist/delete/${id}`)
  }

  public setStatusTrue(id: number): Observable<Todo> {
    return this.http.put<Todo>(`${this.apiServerUrl}/todolist/edit/true/${id}`, id)
  }

  public setStatusFalse(id: number): Observable<Todo> {
    return this.http.put<Todo>(`${this.apiServerUrl}/todolist/edit/false/${id}`, id)
  }

  
}
